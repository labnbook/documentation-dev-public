Technologies utilisées pour l'intégration à Moodle
==================================================

API
---

### Principes de l'API


API REST, avec les verbes GET (lecture), PUT (création), POST (modification).

Dans les requêtes, les données sont soient transmises dans l'URL (obligatoire en GET),
soit dans le corps de la requête, en JSON.

Les réponses sont toujours en JSON.


### Chiffrement

Pour simplifier le développement initial, les échanges sont signés sans cryptographie.
Une clé secrète est associée à l'institution Moodle,
et cette clé doit figurer dans toutes les requêtes à l'API dans le paramètre `key`.

À terme, il faudra sans doute chiffrer au moins une partie du message,
pas pour cacher le contenu mais pour authentifier la source.
Cf `openssl_private_encrypt()` et fonctions voisines.


### API côté LabNbook

Le code est dans `/api/index.php`.
Il utilise le mini-framework [Slim](http://www.slimframework.com/docs/).

Le principal intérêt est de gérer REST au delà du GET et POST natifs en PHP.
Notamment, si on interroge une API en fournissant un bloc de JSON ou XML,
ou en envoyant un fichier, Slim gère
[cela facilement](http://www.slimframework.com/docs/v3/objects/request.html#the-request-body).

L'installation utilise [composer](https://getcomposer.org),
l'outil officiel d'installation de programmes PHP.


### API côté Moodle

La plupart des opérations sont trop sensibles pour pouvoir être exécutées côté client (JS du navigateur).
Donc les requêtes à l'API de LabNbook sont émises en PHP avec cURL.

L'inconvénient est qu'une page web peut demander plusieurs échanges entre Moodle et LnB
avant d'être envoyée au navigateur.
Pour gagner en réactivité, on pourrait dans certains cas chiffrer les paramètres en PHP,
mais envoyer ensuite la requête en JS asynchrone.


Authentification
----------------

Si une personne est authentifiée dans Moodle,
elle n'a pas à s'authentifier à nouveau dans LabNbook quand elle accède à un rapport.

Si les comptes M et L utilisent tous deux une authentification CAS,
alors le passage à L utilise cette dernière.

Sinon, M demande à l'API de L un token permettant d'authentifier automatiquement l'utilisateur.
S'il n'a pas de session, celle-ci sera créée automatiquement.
