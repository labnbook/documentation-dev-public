Intégration de LabNbook à Moodle
================================

Historique des pistes envisagées :

- v0 : norme LTI pour intégration à plusieurs LMS, dont Moodle,
  mais semble incompatible avec le travail en équipe, et surtout ses specs ne sont pas publiques.
- v1 : équivalence entre cours Moodle (format spécifique) et mission LabNbook
- v2 : LabNbook est intégré sous forme d'activité pointant sur une mission


Correspondances entre LnB et Moodle
---------------

| Moodle    | LabNbook |
|-----------|----------|
| catégorie |          |
| cours     |          |
| activité  | mission  |
| groupe    | classe   |
|           | équipe   |

Les équipes de LnB seront implémentées dans Moodle par le plugin.

Les institutions de LnB seront invisibles de Moodle.
Il faudra créer une institution correspondant à l'instance de Moodle.


Toute création d'activité LabnBook dans Moodle crée dans LabnBook :

- une `class` qui sera alimentée par les futures connexions d'étudiants,
  avec un nom déterminé dans Moodle (cours + éventuel groupe) permettant la mise en commun.
  Deux activités du même cours, sans groupe ou avec le même groupe, auront la même classe.
- un `team_config` avec les données de l'étape 1 (à voir comment les saisir)


Cas particulier du Moodle de l'UGA
----------------------------------

En pratique, un cours dans Moodle correspond à une UE.
Les inscrits au cours peuvent donc correspondre à des groupes différents,
avec des périodes de travail distinctes.

Les enseignants dans le Moodle de l'UGA ont généralement accès
à une représentation de leurs classes sous forme de groupe.

Principes
---------

- Création d'un plugin de type activité.
- Chaque instance de cette activité pointe vers une unique mission de LabNbook.
- Chaque mission de LabNbook ne peut être utilisée que par un seul cours de Moodle.
  Cela simplifie la propagation des permissions de Moodle vers LnB,
  et limite les effets de bords des modifications de missions.
- L'instance est configurable dans son ensemble, et éventuellement par groupe.
  Par exemple l'autorisation de l'échange de labdocs déclarée au niveau du groupe
  sera prioritaire sur l'autorisation globale (hors groupes).
- Les permissions sont gérées par Moodle, et transmises à LnB quand nécessaires.
- Les données sont réparties entre M et L :
	- Tout le contenu (rapport et mission) est dans L.
	- Tout le reste est dans M et le plugin (y compris la liste des équipes).
	- En cas de doublon, limiter son usage pour éviter les divergences et synchros.
	  Par exemple, un compte d'utilisateur sera créé à la volée dans LnB,
	  mais caché autant que possible dans l'interface LnB.


Le plugin `mod_labnbook`
------------------------

### Formulaire de création/modification

- Si l'enseignant-éditeur dans ce cours n'a pas de compte dans LnB, on en crée un à la volée.

- *Sélecteur de mission* :
  au minimum, une liste déroulante ; éventuellement, permettre une recherche.
	-  Sélectionner une mission modifie les champs standards "nom" et "description" du formulaire,
	   et signale le choix à LnB (AJAX).
	- ¿ Que faire avec les missions déjà attribuées à une autre cours ?
	  Les afficher, mais désactivées (grisées) ?
	  Les proposer à la sélection en indiquant qu'elles seront dupliquées automatiquement ?

- *Lien vers la création d'une mission*

- Si présence de groupes dans le cours, case à chocher *Configurer par groupe*.


### Page de configuration de la création d'équipes

- Configuration de l'instance.
- Si groupes, ajouter un onglet par groupe,
  avec en première ligne un réglage pour activer sa config spécifique au lieu de celle commune.
- Choix d'un mode de mise en équipe :
	- travail individuel
	- équipes aléatoires
	- choix par les étudiants
	- choix par l'enseignant
- Bloc de réglages dépendant du choix ci-dessus.
- Bloc de réglages communs, appliqué par défaut aux équipes créées : dates, etc

### Page de gestion des équipes

- Liste des équipes-rapports

### Visualisation de l'instance par `view.php`

- Si étudiant à équipes, lien/redirection/iframe vers son rapport.
- Si étudiant sans équipe :
	- on crée son compte LnB s'il n'existe pas encore,
	- on l'affecte automatiquement si possible,
	- si la formation d'équipes est au choix de l'enseignant ou qu'il n'y a plus de place,
	  et si l'enseignant n'est pas encore prévenu (table `warning`),
	  on envoie un message à l'enseignant et on prévient l'étudiant.
- Si enseignant, liste des pages :
	- config de la création d'équipes (avec marqueur si config absente)
	- gestion des équipes présentes (similaire à la page "Rapports" actuelle, mais avec les données de M)
	- liste des étudiants hors équipes (si non-vide)

#### propagation des utilisateurs

Lorsqu'un utilisateur de Moodle veut accéder à LnB
(enseignant créant une activité, étudiant y accédant, etc)  :

- Si son ID dans LabNbook est connu, tout va bien.
- S'il utilise l'authentification CAS,
  ces infos sont transmises à l'API de LnB pour l'identifier.
- S'il a un numéro d'étudiant ?
- Formulaire pour demander s'il a un compte.
    - Si non, un nouveau compte est créé dans LabNbook.
    - Si oui, formulaire d'auth.

**Questions :**

1. Est-ce que l'on marque au fer dans L les utilisateurs ayant une activité Moodle ?
2. Si oui, tous les comptes, ou seulement ceux créés par Moodle ?
3. Est-ce qu'il faut prévoir le cas de plusieurs Moodle devant un même LnB ?

#### propagation des rapports

Pour éviter la complexité et fragilité des synchronisations,
les équipes créées dans Moodle ne chercheront jamais à se rattacher à des rapports existants.
Soit l'équipe est bien déterminée dans Moodle (elle connaît son ID de rapport dans LabNbook),
soit on lui rattache un nouveau rapport créé pour l'occasion.

Pour les utilisateurs, il y a par contre une tentative d'identification
avant de se résoudre à une création de compte LnB.

Il restera à déterminer si l'interface de LabNbook pour un enseignant
affiche les utilisateurs et les rapports associés à Moodle.


#### propagation des droits (pas urgent)*

Tout enseignant-éditeur qui visualise une activité devient co-propriétaire de la mission selon LnB.
Ainsi, une personne qui acquière le rôle d'enseignant-éditeur dans un cours de Moodle
pourra modifier les activités, y compris les missions dans LnB.
Perdre le rôle dans M ne change rien dans L.
Synchro à chaque consultation de l'activité ? (au minimum un cache de session)

NB : dans l'esprit de Moodle, les groupes peuvent servir à faire des équipes.
Depuis M3.6, il y a même un générateur de groupes,
et les devoirs (module "assign") peuvent être rendus par groupe.
Mais si on suit ce principe pour LabNbook, on perd la configuration par classe.


### Affichage de l'instance dans le cours

- Titre et description
- Si une instance n'a pas de config d'équipes, le signaler à l'enseignant par une icône ou une couleur de fond….


### Tables en DB

Préfixe `labnbook_`

- `user` pour les correspondances entre ID d'utilisateurs dans Moodle et LabNbook.
- `team_config` avec un index UNIQUE sur `(instance_id, group_id)` où le second peut être NULL.
- `team` avec clé étrangère sur `team_config.id`
- `warning` : *(non prioritaire)* pour éviter d'envoyer des avertissements en doublon.

### en vrac

- [Doc très incomplète sur la création d'un plugin d'activité](https://docs.moodle.org/dev/Activity_modules)
- [Doc sur le développement dans Moodle, en particulier les plugins](https://docs.moodle.org/dev/Tutorial)
- <https://docs.moodle.org/dev/Adding_a_web_service_to_a_plugin>

Qui émet les requêtes à l'API LnB ?
- Si requêtes en JS, il faut que l'authentification auprès de l'API soit restreinte à l'utilisateur M courant.
- Si requêtes en PHP, alors ce n'est pas nécessaire (un utilisateur de Moodle ne peut pas altérer les paramètres).


Dans LabNbook
-------------

### API

- Utiliser un point d'entrée commun, par exemple `/api/1/index.php`.
- protocole REST
- format JSON

Questions :

- Utiliser un framework PHP ?
- Quelle authentification (de l'utilisateur de Moodle) ?
- Quelle gestion des sessions ? aucune (auth à chaque requête) ?

Les actions nécessaires :

- GET */teacher/mission/list* : liste des missions accessibles à l'utilisateur-enseignant.
- GET */teacher/mission/get*, *id=X* : attributs de la mission
- PUT */teacher/create* : crée un compte d'enseignant dans LnB avec des données de M.
- POST */teacher/mission/grant*, *id=X* : ajoute le droit d'accès sur cette mission de L.
- POST */teacher/mission/use*, *id=X*, *uses=Y* : déclare que la mission est utilisée dans *Y* cours.
- PUT */student/create* : crée un compte d'étudiant dans LnB avec des données de M.

### Création de mission

Paramètre indiquant que l'utilisateur vient de Moodle ("mode Moodle" dans LabNbook).

Lors de la validation en mode Moodle, afficher un message
pour que l'utilisateur retourne dans Moodle et rafraîchisse la liste des missions.

### Modification de mission (non prioritaire)

Actuellement, LabNbook ne prévient que lorsqu'on modifie une mission qui a des rapports en cours.
On peut faire mieux.

La limite « une mission ne peut être utilisée que par un unique cours de Moodle »
évite les situations les plus complexes.

- Hors mode Moodle, si la mission est utilisée dans Moodle, prévenir l'enseignant.
- En mode Moodle, ne prévenir que s'il y a au moins 2 utilisations.
- ¿ Prévenir en cas d'utilisations des deux côtés, cours de Moodle et rapports non-Moodle ?

### Modifications DB

- `report.moodle_team_id UNSIGNED INT NULL DEFAULT NULL`
  permet de distinguer les rapports accessibles via Moodle de ceux qui sont internes.

- `mission.moodle_uses TINYINT NOT NULL DEFAULT 0`
  permet de prévenir en cas de modification d'une mission utilisée.

- `user.moodle_user_id UNSIGNED INT NULL DEFAULT NULL`
  parce que LabNbook a besoin d'un compte utilisateur local pour fonctionner.
