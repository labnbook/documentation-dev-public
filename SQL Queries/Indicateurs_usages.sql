-- *************** RECHERCHE D'UTILISATEUR ****************************
-- ********************************************************************
SELECT * FROM user WHERE user_name LIKE "%cornillon%";
SELECT * FROM user JOIN teacher ON id_teacher = id_user WHERE user_name LIKE "%hansenne%";
SELECT * FROM user WHERE email LIKE "%charbon%";
SELECT * FROM user WHERE id_user = 16980;
SELECT * FROM teacher WHERE id_teacher = 15515;

-- ********* ACTIVITE COURANTE **********
-- **************************************
-- Utilisateurs actuellement connectés (mais pas forcément actifs)
SELECT * FROM user WHERE UNIX_TIMESTAMP() - last_synchro < 60 ORDER BY user_name ;
-- Utilisateurs actifs sur la dernière heure
SELECT id_trace, user.id_user, user_name, first_name, CAST(FROM_UNIXTIME(user.last_synchro) as time) AS last_action -- le last action est faux
	FROM user 
    JOIN trace ON id_trace > 10000000 AND user.id_user = trace.id_user AND TIMESTAMPDIFF(MINUTE,action_time,NOW()) < 60 -- AND id_action IN (6,7,8,9,10,13,15,16,17,18,19,20,21,22,23,24)
    GROUP BY id_user ORDER BY last_synchro DESC;
    
-- ********* ACTIVITE GLOBALE SUR UNE PERIODE **********
-- *****************************************************
-- Nombre de traces représentatives de l'activité des étudiants : edit_ld                                                                                      ! INDICATEUR DE SUIVI
SELECT COUNT(*), MIN(id_trace), MAX(id_trace) FROM labnbook.trace WHERE id_trace >= 11000000 AND id_trace < 13000000 AND action_time >= "2022-11-01" AND action_time < "2022-12-01" AND id_action = 8;

SELECT * FROM trace WHERE action_time >= "2022-01-01 0" LIMIT 1;
SELECT* from trace where id_trace > 10080990;
SET @st_id = 6665010;
SET @end_id = 10080996;
-- Activité par institution : edit_ld
SELECT id_inst, institution.name AS institution, COUNT(*) AS activity FROM trace
	JOIN user USING(id_user) JOIN institution USING(id_inst)
    WHERE id_trace >= @st_id AND id_trace < @end_id AND id_action = 8 AND id_user NOT IN (SELECT id_teacher FROM teacher)
    GROUP BY id_inst ORDER BY activity;
-- Activité pour une institution donnée
SELECT id_mission, code, GROUP_CONCAT(DISTINCT user_name), COUNT(distinct id_trace) AS activity FROM labnbook.trace
	JOIN link_mission_teacher USING(id_mission) JOIN user ON id_teacher = user.id_user JOIN mission USING(id_mission)
    WHERE id_trace >= @st_id AND id_trace < @end_id AND id_action = 8 AND id_inst = 10 AND trace.id_user NOT IN (SELECT id_teacher FROM teacher)
    GROUP BY id_mission ORDER BY activity;

-- Nombre d'utilisateurs différents sur une période
SELECT COUNT(DISTINCT id_user) FROM trace WHERE id_trace > 7000000 AND action_time >= "2021-04-20 0" AND action_time < "2021-11-01";

SET @st_date = 1470009600; -- 1/8/16 
SET @st_date = 1501545600; -- 1/8/17
SET @st_date = 1533081600; -- 1/8/18
SET @st_date = 1564617600; -- 1/8/19
SET @st_date = 1596240000; -- 1/8/20
SET @st_date = 1627768801; -- 1/8/21

SET @end_date = 1470009600; -- 1/8/16 
SET @end_date = 1501545600; -- 1/8/17
SET @end_date = 1533081600; -- 1/8/18
SET @end_date = 1564617600; -- 1/8/19
SET @end_date = 1596240000; -- 1/8/20
SET @end_date = 1627768801; -- 1/8/21
SET @end_date = 1659304801; -- 1/8/22

-- ***************** ACTIVITE MENSUELLE  ***************
-- *****************************************************
select * from trace where id_trace > 10789739 and id_action in (8,9,10,27,28) order by id_user, id_labdoc, id_trace;
select * from trace where action_time > "2022-03-03 17";
SELECT YEAR(action_time), MONTH(action_time), DAY(action_time), COUNT(*) FROM trace WHERE id_trace > 10000000 AND id_action = 27 GROUP BY MONTH(action_time), DAY(action_time) ORDER BY YEAR(action_time), MONTH(action_time), DAY(action_time);

-- ************** ETUDIANTS **********************
-- **********************************************
-- IDEX : Liste des étudiants actif dans une période, filtrables par institution et mission                                      ! INDICATEUR DE SUIVI
SELECT id_inst, user.id_user, first_name, user_name, COUNT(DISTINCT lrl.id_report) AS nb_report, COUNT(id_labdoc) AS nb_LD
	FROM user
	JOIN link_report_learner lrl ON user.id_user = lrl.id_user AND lrl.last_synchro >= @st_date AND lrl.last_synchro < @end_date
    JOIN labdoc l ON l.id_report = lrl.id_report AND l.deleted IS NULL
    -- JOIN report ON lrl.id_report = report.id_report -- utile pour récupérer les id_mission
	WHERE user.id_user NOT IN (SELECT id_teacher FROM teacher)
	AND id_inst IN (2) -- indicateur IDEX
	-- AND id_mission IN (159,481,486) -- les missions choisies
	GROUP BY id_user ORDER BY id_inst, id_user LIMIT 5000;
    
-- Nombre d'étudiants ayant modifié au moins un doc au cours d'une période (à partir de mars 2018) : un peu plus restrictif
SELECT COUNT(DISTINCT trace.id_user) FROM trace
	JOIN user ON trace.id_user = user.id_user
		-- AND user.id_inst IN (2)
	WHERE id_action = 9 AND action_time >= "2019-08-01" AND action_time < "2020-08-01"
    AND trace.id_user NOT IN (SELECT id_teacher FROM teacher);

-- Nombre d'étudiants actif dans une période, triés par institution
SELECT i.name, COUNT( DISTINCT user.id_user) AS nb_students
	FROM institution i
    JOIN user ON user.id_inst = i.id_inst AND user.id_user NOT IN (SELECT id_teacher FROM teacher)
	JOIN link_report_learner lrl ON user.id_user = lrl.id_user AND lrl.last_synchro >= @st_date AND lrl.last_synchro < @end_date
	GROUP BY i.id_inst ORDER BY i.id_inst;

-- Temps d'écriture des étudiants, année par année - on divise par 3 & 60 pour avoir le temps en heures                                      ! INDICATEUR DE SUIVI
-- on divise par le nombre d'équipes pour le temps moyen 
SELECT COUNT(id_trace)/3/60 AS "temps d'écriture (h)" FROM trace
	JOIN user USING(id_user)
    WHERE id_user NOT IN (SELECT id_teacher FROM teacher)
    AND id_trace > 4000000
	AND id_action = 9
    -- AND id_inst IN(2)
    AND action_time >= "2021-08-01" AND action_time < "2022-08-01";

-- ************** ENSEIGNANTS ********************
-- ***********************************************
-- table lb_tracks < 3/3/18 : Nombre total d'enseignants connectés dans une période, filtrables par institution
SELECT DISTINCT id_user FROM trace_old
	WHERE action_time >= "2016-08-01" AND action_time < "2017-08-01"
    AND trace_old.id_user IN (
		SELECT DISTINCT teacher.id_teacher FROM teacher
        JOIN link_inst_teacher USING(id_teacher)
        JOIN link_mission_teacher USING(id_teacher)
		JOIN link_class_teacher USING(id_teacher)
		JOIN class ON class.id_class = link_class_teacher.id_class AND class.id_inst = link_inst_teacher.id_inst
		JOIN link_class_learner lcl ON class.id_class = lcl.id_class AND lcl.id_user NOT IN (SELECT id_teacher FROM teacher)
        WHERE link_inst_teacher.id_inst IN (2,3)
	);

-- table traces > 3/3/18 : Nombre total d'enseignants connectés dans une période, filtrables par institution                      ! INDICATEUR DE SUIVI
SELECT DISTINCT trace.id_user -- , first_name, user_name
	FROM trace
    -- JOIN user USING(id_user)
	JOIN teacher ON id_user = id_teacher
    JOIN link_inst_teacher lit USING(id_teacher)
	JOIN link_mission_teacher USING(id_teacher) -- au moins une mission
	JOIN link_class_teacher lct USING(id_teacher)   -- au moins une classe
	JOIN class ON class.id_class = lct.id_class AND class.id_inst = lit.id_inst
	JOIN link_class_learner lcl ON class.id_class = lcl.id_class AND lcl.id_user NOT IN (SELECT id_teacher FROM teacher) -- au moins un étudiant dans la classe
    WHERE id_trace > 4000000 AND action_time >= "2021-08-01" AND action_time < "2022-08-01"
    -- AND lit.id_inst IN (2)
;

-- IDEX-total
SELECT DISTINCT tbl.id_user FROM (
	SELECT id_user 
		FROM trace_old
		WHERE action_time >= "2017-08-01" -- AND action_time < "2019-08-01"
	UNION
	SELECT id_user
		FROM trace
		-- WHERE action_time < "2018-08-01" -- AND action_time >= "2019-08-01"
) tbl
	JOIN teacher ON tbl.id_user = id_teacher
    JOIN link_inst_teacher lit USING(id_teacher)
	JOIN link_mission_teacher USING(id_teacher) -- au moins une mission
	JOIN link_class_teacher lct USING(id_teacher)   -- au moins une classe
	JOIN class ON class.id_class = lct.id_class AND class.id_inst = lit.id_inst
	JOIN link_class_learner lcl ON class.id_class = lcl.id_class AND lcl.id_user NOT IN (SELECT id_teacher FROM teacher) -- au moins un étudiant dans la classe
	WHERE lit.id_inst IN (2,3)
;

-- Caractéristiques de tous les enseignants, connectés depuis une date (last_synchro), filtrables par institution
SELECT institution.name AS institution, user.id_user, user_name, first_name, teacher_domain, teacher_level, teacher_comment, CAST(FROM_UNIXTIME(user.last_synchro) as date) as last_synchro, COUNT(DISTINCT id_mission) AS nb_missions, COUNT(DISTINCT class.id_class) AS nb_class, COUNT(DISTINCT link_class_learner.id_user) AS nb_students
	FROM teacher
	JOIN user ON id_teacher = id_user
	JOIN link_inst_teacher USING(id_teacher)
	JOIN institution ON link_inst_teacher.id_inst = institution.id_inst
	LEFT JOIN link_mission_teacher USING(id_teacher)
	LEFT JOIN link_class_teacher ON id_user = link_class_teacher.id_teacher
	LEFT JOIN class ON class.id_class = link_class_teacher.id_class AND class.id_inst = link_inst_teacher.id_inst
	LEFT JOIN link_class_learner ON class.id_class = link_class_learner.id_class
	WHERE last_synchro >= @st_date
	   -- AND institution.id_inst IN (2,3)
	GROUP by user.id_user, institution.id_inst
	ORDER BY link_inst_teacher.id_inst, id_user ;
    
-- Caractéristiques des enseignants ACTIFS (mission>0 ; classes>0 ; étudiants>0), connectés depuis une date (last_synchro), filtrables par institution
SELECT institution.name AS institution, user.id_user, user_name, first_name, teacher_comment, user.creation_time, CAST(FROM_UNIXTIME(user.last_synchro) as date) as last_synchro, COUNT(DISTINCT id_mission) AS nb_missions, COUNT(DISTINCT class.id_class) AS nb_class, COUNT(DISTINCT link_class_learner.id_user) AS nb_students
	FROM teacher
	JOIN user ON id_teacher = id_user
	JOIN link_inst_teacher USING(id_teacher)
	JOIN institution ON link_inst_teacher.id_inst = institution.id_inst
	JOIN link_mission_teacher USING(id_teacher)
	JOIN link_class_teacher ON id_user = link_class_teacher.id_teacher
	JOIN class ON class.id_class = link_class_teacher.id_class AND class.id_inst = link_inst_teacher.id_inst
	JOIN link_class_learner ON class.id_class = link_class_learner.id_class
	WHERE last_synchro >= @st_date
	   AND institution.id_inst IN (2,3)
	GROUP by user.id_user, institution.id_inst
	ORDER BY link_inst_teacher.id_inst, id_user ;
    
-- Caractéristiques des PRINCIPAUX enseignants ACTIFS (mission>0 ; classes>0 ; étudiants>0), connectés depuis une date (last_synchro), filtrables par institution
SELECT institution.name AS institution, user.id_user, user_name, first_name, teacher_comment, CAST(FROM_UNIXTIME(user.last_synchro) as date) as last_synchro, COUNT(DISTINCT id_mission) AS nb_missions, COUNT(DISTINCT class.id_class) AS nb_class, COUNT(DISTINCT link_class_learner.id_user) AS nb_students
	FROM teacher
	JOIN user ON id_teacher = user.id_user
	JOIN institution USING(id_inst)
	JOIN link_mission_teacher USING(id_teacher)
	JOIN link_class_teacher ON id_user = link_class_teacher.id_teacher
	JOIN class ON class.id_class = link_class_teacher.id_class AND class.id_inst = user.id_inst
	JOIN link_class_learner ON class.id_class = link_class_learner.id_class
	WHERE last_synchro >= @st_date
	-- AND institution.id_inst IN (2,3)
	GROUP by user.id_user, institution.id_inst
	ORDER BY user.id_inst, id_user ;
    
-- activité mensuelle des enseignants (nb de connexions) --> Heatmap
SELECT		id_user, YEAR(action_time) as y, MONTH(action_time) as m, COUNT(id_action)
FROM		user 
JOIN		trace USING(id_user)
WHERE		id_user IN (SELECT id_teacher FROM teacher)
			AND id_action = 1 -- connexion à LNb
            AND YEAR(action_time) > 2019
GROUP BY	id_user, YEAR(action_time), MONTH(action_time)
ORDER BY	id_user
LIMIT 		3000;

-- ************** MISSIONS ********************
-- ********************************************
-- caractéristiques des missions avec noms des enseignants filtrables par institution
SELECT mission.id_mission, code, mission.name, creation_date, modif_date, COUNT(DISTINCT r.id_report) AS nb_reports, GROUP_CONCAT(DISTINCT user_name) AS designers
	FROM mission
    JOIN link_mission_teacher lmt ON lmt.id_mission = mission.id_mission AND teacher_type = "designer"
	JOIN user ON user.id_user = lmt.id_teacher AND user.id_inst IN (2)
    JOIN report r ON r.id_mission = mission.id_mission
    JOIN link_report_learner lrl ON r.id_report = lrl.id_report AND lrl.last_synchro >= @st_date AND lrl.last_synchro < @end_date AND lrl.id_user NOT IN (SELECT id_teacher FROM teacher)
	GROUP BY id_mission ORDER BY code LIMIT 1000;

-- caractéristiques des missions avec le nombre d'étudiants filtrables par institution                                                                              ! INDICATEUR DE SUIVI
SELECT mission.id_mission, code, mission.name, creation_date, modif_date, COUNT(DISTINCT r.id_report) AS nb_reports, COUNT(DISTINCT lrl.id_user) AS nb_students
	FROM mission
	JOIN report r USING(id_mission)
	JOIN link_report_learner lrl ON lrl.id_user NOT IN (SELECT id_teacher FROM teacher) AND r.id_report = lrl.id_report AND lrl.last_synchro >= @st_date -- AND lrl.last_synchro < @end_date
	JOIN user ON lrl.id_user = user.id_user
    -- AND user.id_inst IN (2)
	GROUP BY id_mission
    HAVING nb_students > 5;
    
-- Utilisation de la table de traces pour sélectionner les missions les plus utilisées au cours d'une période
SELECT id_mission, code, name, creation_date, modif_date, COUNT(DISTINCT id_report) AS nb_reports, COUNT(DISTINCT id_user) AS nb_students
FROM trace JOIN mission USING(id_mission)
WHERE id_action = 3 -- cnx à un rapport
AND action_time >= "2018-08-01"
AND id_user NOT IN (SELECT id_teacher FROM teacher)
GROUP BY id_mission
HAVING nb_students > 19;


-- ************** RAPPORTS ********************
-- ********************************************
-- nombre de rapports modifiés durant une période, filtrables par institution                                                                                       ! INDICATEUR DE SUIVI
SELECT COUNT(DISTINCT r.id_report)
	FROM report r
	JOIN link_report_learner lrl ON r.id_report = lrl.id_report AND lrl.last_synchro >= @st_date AND lrl.last_synchro < @end_date AND lrl.id_user NOT IN (SELECT id_teacher FROM teacher)
	JOIN user ON lrl.id_user = user.id_user
    -- AND user.id_inst IN (2)
    ;

-- ************** LABDOCS ********************
-- *******************************************
-- Nb de chaque type de LB créés par des enseignants dans les missions et ayant été utilisés depuis la date considérée
SELECT type_labdoc, count(*)
	FROM labdoc
	WHERE id_report IS NULL AND id_report_part IS NOT NULL AND position IS NOT NULL
	AND labdoc.last_edition >= @st_date AND labdoc.last_edition < @end_date
	GROUP BY type_labdoc ;

-- Nb de chaque type de LB créés / utilisés par des étudiants dans les rapports et ayant été utilisés depuis la date considérée
SELECT type_labdoc, count(DISTINCT id_labdoc) AS nb_ld
	FROM labdoc
    JOIN report USING(id_report) -- pour le filtrage par mission
    JOIN link_report_learner USING(id_report)
    JOIN user ON user.id_user = link_report_learner.id_user AND id_inst IN(2,3)
    WHERE id_report IS NOT NULL AND id_report_part IS NOT NULL AND position IS NOT NULL AND deleted IS NULL
	-- AND id_ld_origin IS NULL -- uniquement les labdocs créés par les étudiants / commenter cette ligne pour avoir TOUS les labdocs utilisés par les étudiants
    AND id_mission IN (629,642,669,671,923,991,994)  -- filtrage par mission
	AND last_editor NOT IN (SELECT id_teacher FROM teacher)
	AND labdoc.last_edition >= @st_date AND labdoc.last_edition < @end_date
	GROUP BY type_labdoc ORDER BY nb_ld DESC;

-- ************** EQUIPES ********************
 -- ******************************************
-- Liste des rapports modifiés dans la période avec le nb d'étudiants de l'équipe
SELECT report.id_report, COUNT(DISTINCT id_user) AS nb_étudiants
FROM report
JOIN link_report_learner lrl ON report.id_report = lrl.id_report AND lrl.last_synchro >= @st_date AND lrl.last_synchro < @end_date AND lrl.id_user NOT IN (SELECT id_teacher FROM teacher)
JOIN user USING(id_user) -- les étudiants propriétaires de ces rapports
WHERE user.id_user NOT IN (SELECT id_teacher FROM teacher) -- qui ne sont pas des enseignants
GROUP BY report.id_report ORDER BY id_report LIMIT 10000;

--  types de mises en équipe
-- sans les traces du côté des enseignants, donc il a fallu ruser. 
-- 1 : comptage des rapports dont le couple (mission, classe d'un des membres) est le même qu'une config de mise en équipes.
SELECT count(DISTINCT r.id_report)
	FROM report r
	JOIN team_config tc ON r.id_mission = tc.id_mission
	JOIN link_report_learner lrl USING(id_report)
	JOIN link_class_learner lcl ON lrl.id_user = lcl.id_user
	WHERE lcl.id_class = tc.id_class
	AND r.creation_time >  tc.creation_time
	AND r.creation_time > '2018-10-01';
-- 2 : équipes créées de façon simultanée.
SELECT SUM(c)
	FROM (
	SELECT count(*) AS c
	FROM report
	WHERE creation_time > '2018-10-01'
	GROUP BY id_mission, creation_time
	HAVING count(*) > 1
	) AS f;
-- 4 : 
SELECT count(DISTINCT r.id_report)
	FROM report r
	WHERE r.creation_time > '2018-10-01'
;

-- Estimations pour les équipes formées depuis le 1er octobre 2018 (juste après l'introduction du nouveau système) :
-- 1 : 825 équipes en auto-création
-- 2 : 1206 par tirage aléatoire ou par récupération d'une mise en équipe d'une autre mission
-- 3 : 768 par choix de l'enseignant
-- 4 : total de 2799

-- ************** ANNOTATIONS ********************
-- ***********************************************
-- Nb d'annotations durant une période                                                       ! INDICATEUR DE SUIVI
SELECT COUNT(id_annotation)
	FROM annotation a
    JOIN user ON user.id_user = a.id_teacher
    -- AND user.id_inst IN (2)
    WHERE a.creation_time >= '2021-08-01' ;  -- AND a.creation_time < '2020-08-01';

-- Nb d'annotations par mission
SELECT code, count(DISTINCT id_annotation) AS nb_annotation
	FROM annotation 
	LEFT JOIN report USING(id_report)
	LEFT JOIN mission USING(id_mission)
	GROUP BY id_mission;
    
-- ************** MESSAGES *************************
-- *************************************************
SELECT COUNT(id_message) FROM message
	-- JOIN user ON user.id_user = message.id_user_sender
	WHERE 1
    AND id_user_sender <> 1
    AND id_user_sender NOT IN (SELECT id_teacher FROM teacher)
    -- AND user.id_inst IN (2)
    AND msg_time >= '2020-08-01' ; -- AND msg_time < '2020-03-24';

-- ************** COMMENTAIRES *************************
-- *****************************************************
SELECT COUNT(id_comment) FROM comment
	-- JOIN user ON user.id_user = comment.id_user
    WHERE 1
    AND id_user <> 1
    -- AND user.id_inst IN (2)
	AND comment_time >= '2020-08-01' ; -- AND comment_time < '2019-03-24';