
-- test du fonctionnement des traces
select trace_action.*, count(id_trace) as count from trace_action
left join trace on trace_action.id_action = trace.id_action and id_trace > 7665010 -- mars 2021
group by id_action order by count;
select * from trace where id_trace = 7665010;

-- *******************************************************************************************
--                EXPORT DES TRACES
-- *******************************************************************************************

-- 1 : sur MySQL WorkBench
CREATE TABLE trace_temp
	SELECT id_trace, action_time, id_user, researcher AS user_type, teacher_type, trace.id_mission, id_report, id_labdoc, id_action
    FROM trace 
    LEFT JOIN teacher ON trace.id_user = teacher.id_teacher
    LEFT JOIN link_mission_teacher lmt ON lmt.id_mission = trace.id_mission AND lmt.id_teacher = trace.id_user
    ORDER BY id_mission, id_report, id_trace;
SELECT COUNT(*) FROM trace_temp;
SELECT * FROM trace_temp LIMIT 500;

-- 2 : sur adminer
-- Se connecter sur https://uga.labnbook.fr/adminer et utiliser la connexion : labnbook-ro / thoh7Pha5Aen9Aiz
-- Exporter la table complète en CSV

-- 3 : sur MySQL WorkBench
DROP TABLE trace_temp;