
-- *************** RECHERCHE D'UTILISATEUR ****************************
-- ********************************************************************
SELECT * FROM user WHERE user_name LIKE "%lerouxel%";
SELECT * FROM user WHERE email LIKE "severine.blanchon@ac-grenoble.fr";
SELECT * FROM user JOIN teacher ON id_teacher = id_user WHERE user_name LIKE "%blanchon%";
SELECT * FROM user JOIN teacher ON id_user = id_teacher WHERE email LIKE "%cpe%";
SELECT * FROM user WHERE id_user = 12118001;
SELECT * FROM teacher WHERE id_teacher = 2469;


-- *************** PASSAGE EN MODE TEACHER (3 ou 4 requêtes) *********************
-- *******************************************************************************
-- il faut d'abord ajouter comme étudiant dans la classe adaptée
-- puis récupérer l'id_useruser
SELECT * FROM user ORDER BY id_user DESC LIMIT 200;
SET @id_user = 16962;
SELECT * FROM user WHERE id_user = @id_user; -- vérifier qu'on ne se trompe pas d'utilisateur !!
-- teacher_level = Lycee CPGE Sup
-- teacher_comment = Ville-établissement
INSERT INTO `teacher` (`id_teacher`, `teacher_domain`, `teacher_level`, `teacher_comment`)
	VALUES            (@id_user    , ''              , 'Univ'        , 'IMT');
INSERT INTO link_inst_teacher (id_teacher, id_inst) SELECT id_user, id_inst FROM user WHERE id_user = @id_user;
-- ATTENTION : Uniquement pour les gestionnaires pédagogiques
UPDATE teacher SET role = "manager" WHERE id_teacher = @id_user;
-- ATTENTION : Uniquement pour un login AGALAN
-- UPDATE user SET pwd = NULL WHERE id_user = @id_user;

-- il faut envoyer le mail depuis les modèles
-- il faut ajouter l'adresse mail à la liste de diffusion
SELECT id_user, email, pwd, teacher_comment, creation_time FROM user JOIN teacher ON id_user = id_teacher WHERE email <> '' ORDER by id_teacher DESC LIMIT 100;

-- les derniers enseignants ajoutés 
SELECT user.id_user, user.user_name, user.first_name, institution.name AS inst, user.id_inst, user.email, user.login, user.pwd, user.creation_time, user.last_synchro, teacher_comment
	FROM teacher JOIN user ON id_teacher = id_user JOIN institution USING (id_inst)
	ORDER BY id_teacher DESC ;
    
-- Passer un teacher en manager
SELECT * FROM teacher JOIN user ON id_user = id_teacher WHERE user_name LIKE "%burtin%";
UPDATE teacher SET role="manager" WHERE id_teacher = 12249;

-- *********************** MODIFICATION D'UN MOT DE PASSE ****************************
-- ***********************************************************************************
-- sha256 mdp initial = 
-- sha256("mdp") = f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17
UPDATE user SET pwd = "f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17" WHERE id_user = 7;


-- ************************ RECUPERATION DE TRACES ***********************************
-- ***********************************************************************************
 -- Traces des utilisateurs. teacher : NULL = étudiant / 1 = teacher
 -- Requête rapide 
 SELECT id_trace, action_time, id_user, id_mission, id_report, id_labdoc, id_action, CAST(attributes AS CHAR(255) CHARSET utf8) AS attr
	-- , id_teacher > 0 AS teacher
	FROM trace t
	-- LEFT JOIN teacher ON t.id_user = teacher.id_teacher -- utile pour le statut teacher
	-- JOIN link_class_learner USING (id_user)
	WHERE 1 
    -- AND action_time > "2020-09-09"
	-- AND id_user IN (3579)
	-- AND id_teacher IS NULL
	-- AND id_mission IN (1118, 1119, 1120, 1121)
	AND id_report = 52040 
	-- AND t.id_labdoc = 332951
	-- AND id_action = 46
	-- AND id_class = 825
    -- AND id_trace > 6000000
	ORDER BY id_trace DESC LIMIT 4000;
    
 -- Requête complète ATTENTION, ça peut faire planter LNb
SELECT id_trace, action_time, id_user, id_mission, code, id_report, id_labdoc, id_action, action_type, action, CAST(attributes AS CHAR(255) CHARSET utf8) AS attr
	-- , id_teacher > 0 AS teacher
	FROM trace t
    JOIN trace_action ta USING (id_action)
	-- LEFT JOIN teacher ON t.id_user = teacher.id_teacher -- utile pour le statut teacher
	LEFT JOIN mission USING (id_mission) -- pour le code
	-- JOIN link_class_learner USING (id_user)
	WHERE 1
	-- AND id_trace = 5539280
    -- AND action_time > "2020-09-09"
	 AND id_user IN (3579)
	-- AND id_teacher IS NULL
	-- AND id_mission IN (1118, 1119, 1120, 1121)
	-- AND id_report = 45267  
	-- AND t.id_labdoc = 444896
	-- AND id_action > 30
	-- AND id_class = 825
    -- AND id_trace > 6000000
	ORDER BY id_trace DESC LIMIT 4000;
    
-- ****************************** INFOS D'UN ETUDIANT **************************
-- *****************************************************************************
-- les productions d'1 étudiant
SELECT user.user_name, user.first_name, report.id_report, mission.id_mission, mission.code, labdoc.id_labdoc, labdoc.name, FROM_UNIXTIME(labdoc.last_edition), labdoc.deleted
	FROM user JOIN link_report_learner USING (id_user) JOIN report USING (id_report) JOIN mission USING (id_mission) JOIN labdoc USING (id_report)
	WHERE user.id_user = 8877
	ORDER BY id_report, labdoc.deleted, labdoc.name;
 
 -- les mises en équipe d'1 étudiant
SELECT tc.* FROM link_class_learner lcl
	JOIN team_config tc USING(id_class)
	WHERE id_user = 12056;
 
-- les classes et enseignants d'1 étudiant
SELECT u.user_name, u.first_name, id_class, class_name, institution.name AS institution, GROUP_CONCAT(t.first_name) AS ens_p, GROUP_CONCAT(t.user_name) AS ens_n
	FROM user u JOIN link_class_learner USING(id_user) JOIN class USING(id_class) JOIN institution ON class.id_inst = institution.id_inst
	JOIN link_class_teacher USING(id_class) JOIN teacher USING (id_teacher) JOIN user t ON teacher.id_teacher = t.id_user
	WHERE u.id_user = 12056
	GROUP BY id_class;

-- les missions et tuteurs d'1 étudiant
SELECT u.user_name, u.first_name, code AS mission, id_report, GROUP_CONCAT(t.user_name)  AS tuteurs
	FROM user u JOIN link_report_learner USING(id_user) JOIN report USING(id_report) JOIN mission USING(id_mission)
	JOIN link_mission_teacher USING(id_mission) JOIN teacher USING (id_teacher) JOIN user t ON teacher.id_teacher = t.id_user
	WHERE u.id_user = 11071
	GROUP BY id_mission;

-- ****************************** INFOS D'UN RAPPORT ***************************
-- *****************************************************************************
SELECT r.id_mission, m.code, r.id_report, r.start_datetime, r.end_datetime, r.status, r.team_name, lrl.id_user, u.last_synchro, u.user_name, u.first_name, u.current_report, u.last_synchro
	FROM link_report_learner lrl JOIN user u USING (id_user) JOIN report r USING (id_report) JOIN mission m USING (id_mission)
	WHERE id_report = 52040;

-- ****************************** INFOS D'UN TEACHER ***************************
-- *****************************************************************************
-- Infos de base pour 1 teacher
SELECT * FROM teacher JOIN user ON id_teacher = id_user JOIN link_inst_teacher lit USING(id_teacher) JOIN institution i ON lit.id_inst = i.id_inst
    WHERE id_teacher = 9291;

-- les missions d'1 teacher
SELECT id_user, first_name, user_name, mission.id_mission, mission.code, mission.name, mission.creation_date, mission.modif_date, COUNT(id_report) AS nb_reports
	FROM user
    JOIN link_mission_teacher ON id_user = id_teacher
    JOIN mission USING (id_mission)
    JOIN report USING (id_mission)
	WHERE id_teacher = 9291 AND report.status <> "new" AND report.status <> "test" GROUP BY id_mission;
 
-- les classes d'1 teacher
SELECT id_teacher, first_name, user_name, class.id_class, class_name, class.creation_time, class.id_inst, COUNT(link_class_learner.id_user) AS nb_learners
	FROM teacher
	JOIN user ON teacher.id_teacher = user.id_user
	JOIN link_class_teacher USING(id_teacher)
	JOIN link_class_learner USING (id_class)
	JOIN class USING (id_class)
	WHERE id_teacher = 9291 GROUP BY id_class;

-- ****************************** INFOS D'UNE MISSION **************************
-- *****************************************************************************
-- les classes d'une mission
SELECT mission.code, id_mission, id_class, class_name, class.creation_time, COUNT(DISTINCT id_user) AS nb_learners
	FROM mission JOIN report USING(id_mission) JOIN link_report_learner USING(id_report) JOIN link_class_learner USING(id_user) JOIN class USING(id_class)
	WHERE id_mission IN (693)
	GROUP BY id_class ORDER BY code;

-- ****************************** MAINTENANCE DB *******************************
-- *****************************************************************************
-- Suppression des LD créés inutilement dans les missions
SELECT * FROM labdoc WHERE deleted IS NOT NULL AND last_edition IS NULL;
-- DELETE FROM labdoc WHERE deleted IS NOT NULL AND last_edition IS NULL;

-- Suppression des LD créés inutilement dans les rapports
SELECT * FROM labdoc WHERE deleted IS NOT NULL AND labdoc_data = "";
-- DELETE FROM labdoc WHERE deleted IS NOT NULL AND labdoc_data = "";

-- Suppression des LD supprimés depuis plus de 100 jours
SELECT * FROM labdoc WHERE deleted IS NOT NULL AND last_edition < (UNIX_TIMESTAMP()-100*24*3600);
-- DELETE FROM labdoc WHERE deleted IS NOT NULL AND last_edition < (UNIX_TIMESTAMP()-100*24*3600);

-- Processus en cours (si besoin ouvrir une nouvelle connection à la DB)
SHOW PROCESSLIST;

-- Arrêter un processus
   KILL ID-du-processus;