-- Les requêtes pour estimer les activités par institution et par année universitaire

-- Nb de comptes étudiants actifs (trace 3 : enter_report)
SELECT
CASE
    WHEN MONTH(action_time) < 8 THEN CONCAT(YEAR(action_time)-1, "-", YEAR(action_time))
    ELSE CONCAT(YEAR(action_time), "-", YEAR(action_time)+1)
END
AS u_year, COUNT(DISTINCT id_user)
FROM trace JOIN user USING(id_user) 
WHERE action_time > "2018-08-01 00:00:00" AND id_inst = 2 AND id_action = 3 AND id_user NOT IN (SELECT id_teacher FROM teacher)
GROUP BY u_year;

-- Nb de comptes enseignants actifs (traces 50 ou 31 : display_report_info ou follow_report)
SELECT
CASE
    WHEN MONTH(action_time) < 8 THEN CONCAT(YEAR(action_time)-1, "-", YEAR(action_time))
    ELSE CONCAT(YEAR(action_time), "-", YEAR(action_time)+1)
END
AS u_year, COUNT(DISTINCT id_user)
FROM trace JOIN user USING(id_user) 
WHERE action_time > "2018-08-01 00:00:00" AND id_inst = 2 AND id_action IN(31,50) AND id_user IN (SELECT id_teacher FROM teacher)
GROUP BY u_year;

-- Nb de rapports actifs (trace 3 : enter_report)
SELECT
CASE
    WHEN MONTH(action_time) < 8 THEN CONCAT(YEAR(action_time)-1, "-", YEAR(action_time))
    ELSE CONCAT(YEAR(action_time), "-", YEAR(action_time)+1)
END
AS u_year, COUNT(DISTINCT trace.id_report)
FROM trace JOIN user USING(id_user) 
WHERE action_time > "2018-08-01 00:00:00" AND id_inst = 2 AND id_action = 3
GROUP BY u_year;