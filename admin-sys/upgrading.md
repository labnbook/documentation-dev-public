# LabNbook : montées de version

## Préalable avec git

1. Vérifier que la branche `master` locale est à jour
2. Vérifier que la branche `production` locale est à jour
3. Fusionner `master` dans `production` et publier

```
git checkout master && git pull --rebase && git push
git log --oneline --merges production..master
git checkout production && git pull && git merge -m "merge master into production" master && git push && git checkout -
```

## Sur le serveur

Connexion par `ssh <server>`.

Si opération s'annonce longue,
activer préalablement le mode de maintenance dans `common/cnx_var.php`.

1. sauvegarde SQL
2. mise à jour du code source
3. si tout va bien, migration des données
4. éventuellement, renommer les fichiers de logs

```
cd www
git fetch -p
mysqldump labnbook |gzip > ~/labnbook_pre-upgrade_$(date "+%F_%R").sql.gz

git pull && php7.0 scripts/migrate.php -y
mv files/js_errors.log files/js_errors_$(date +"%Y-%m-%d").log
mv files/mysql_errors.log files/mysql_errors_$(date +"%Y-%m-%d").log
```

