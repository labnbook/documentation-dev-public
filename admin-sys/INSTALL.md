Installing labNbook
===================

This is for a manual installation.
An automatic installation with Ansible is provided in a separate repository.


Requirements
------------

- PHP == 8.1
- MySQL 5 minimum (tested with MariaDB 10.1 and 10.3)
- PHP extensions: bcmath, ctype, curl, fpm, ldap, mbstring, pdo-mysql, tokenizer, xml, zip, sodium
- nodejs >= 14
- yarn
- chromium

Installation
------------

1. Get the source code:
   `git clone git@gricad-gitlab.univ-grenoble-alpes.fr:labnbook/labnbook.git && cd labnbook`

### Docker install

If a `docker-compose.yml` file is present in your branch, you can use this method for both labnbook legacy and laravel

1. Install [docker](https://docs.docker.com/install/) and [docker-compose](https://docs.docker.com/compose/install)
2. Copy `cp .env.sample .env`
3. Edit `.env` and change passwords
4. Run `./lnb up`  this will start the serveur and pull the dependencies (TODO add automatic key app generation `php artisan key:generate`)
5. Wait for the databse to be ready
6. Run `./lnb artisan key:generate`
7. Run `./lnb perms`
8. Restart the stack : Ctrl+C then `./lnb up`

TODO : step 9 is broken

9. Populate the database `./lnb mysql_init`
9. bis : Populate database with test data `./lnb mysql_load_test_data`
10. Go to http://localhost:8080 and login as `admin` with the following password: `mdp`
11. Go to the admin page and check that install is successfull (you'll probably be notified to run `./lnb migrate`)

### Local install

1. Create the database and a mysql user:
	```
	echo "CREATE DATABASE labnbook DEFAULT CHARACTER SET 'utf8mb4' DEFAULT COLLATE 'utf8mb4_unicode_ci';" | mysql
	echo "GRANT ALL PRIVILEGES ON labnbook.* TO labnbook@localhost IDENTIFIED BY 'monmdp';" | mysql
	```

2. Load the DB dump: `zcat dump.sql.gz | mysql labnbook`
   Cleaning the dump may be useful (change charsets, etc).

3. Copy `.env.sample` to `.env` and edit it (change password)

4. To install Labnbook dependencies, you need [composer](https://getcomposer.org/download/)
and [yarn](https://yarnpkg.com/en/docs/install)

5. run `composer install`

5. Generate laravel key `php artisan key:generate` and public storage link `php artisan storage:link`

7. Run the configuration script : `./refresh.sh` that will install composer and yarn dependencies and run migrations

8. Ensure `php-fpm` (or `mod_php`) is correctly configured.
   Debian fpm: check `/etc/php/7.0/fpm/pool.d/www.conf` (default pool),
   especially the directives `listen` and `pm`,
   then check `/etc/php/7.0/fpm/php.ini`.

   To allow posting large files, configure the webserver (see usual parameters of Apache, Nginx, etc),
   and the PHP pool. E.g. in `php.ini`:
   ```
   # nginx
   client_max_body_size 64M;

   # apache 2.4
   # Not needed because default is 0=unlimited
   #LimitRequestBody 67108864

   # php.ini
   post_max_size = 64M
   upload_max_filesize = 64M
   ```

9. Increase the MySQL max size of queries, and be more lenient on the SQL syntax.
   Add to the config, e.g. `/etc/mysql/conf.d/labnbook.conf`:
	```
	[mysqld]
	max_allowed_packet=64M
	" Oracle mysql is too strict for labnbook
	sql-mode="STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION"
	```

10. Configure your webserver : if you are developping just use laravel's `php artisan serve`, else see the end of the legacy local install instructions

11. Make sure that `/usr/bin/chromium` points to chromium, this is needed for pdf printing
