# Sauvegardes périodiques

## Ce qui est en place

Sur le serveur de production `uga.labnbook.fr` :

- Les sauvegardes sont dans `/var/lib/automysqlbackup`.
- Seul l'utilisateur *root* y a accès.
- Les sauvegardes sont réalisées à 6h25 chaque matin.

Ce sont des sauvegardes étagées : on garde les 7 dernières sauvegardes quotidiennes,
puis les 3 des débuts de semaines précédentes, puis celles des débuts de mois.
Par exemple, `/var/lib/automysqlbackup/daily/labnbook/labnbook_2018-10-31_06h25m.mercredi.sql.gz`.

Les sauvegardes quotidiennes sont **répliquées sur le serveur de test** à 6h50.
On y accède par le compte applicatif normal `labnbook@labnbook-test.imag.fr` dans son répertoire
`backup-prod/data/labnbook`.

## Comment restaurer

**Attention**, les vieilles sauvegardes (jusqu'à août 2018), contiennent une directive `USE`
dans le SQL, donc elles se restaurent systématiquement dans la base `labnbook`.
Pour plus de sécurité, vérifier avec `zless` les premières lignes de la sauvegardes,
et utiliser un compte MySQL qui n'a pas accès à la base de prod.

### Restauration ciblée

Pour une restauration ciblée (mission ou rapport), on peut le faire hors du serveur de prod.

- Récupérer une savegarde SQL et la déployer dans son instance locale.
- Sauvegarder la cible, et copier les fichiers produits sur le serveur de prod
- En prod restaurer.

```
scp labnbook@labnbook-test.imag.fr:backup-prod/data/labnbook/labnbook_2018-11-02_06h25m.vendredi.sql.gz tmp/
zless tmp/labnbook_2018-11-02_06h25m.vendredi.sql.gz
zcat tmp/labnbook_2018-11-02_06h25m.vendredi.sql.gz | mysql labnbook

php5.6 scripts/mission.php backup 703
tar czf backup_mission_703.tgz files/backup/mission/703
scp backup_mission_703.tgz labnbook@uga.labnbook.fr:

ssh labnbook@uga.labnbook.fr
  cd www
  tar xf ~/backup_mission_703.tgz
  php5.6 scripts/mission.php restore 703
```

La restauration reprend les ID de la sauvegarde.
Elle n'est donc pas adapté pour copier un objet sur une instance sans rapport (risque d'écrasement).

### Restauration globale ou autre

Pour éviter les conflits pendant une restauration globale,
il faut bloquer temporairement l'accès web,
cf la configuration dans `common/cnx_var.php`.

Pour des actions sur mesure, il peut être pratique d'agir sur la base de prod
en utilisant des données de la sauvegarde.

```
echo "DROP DATABASE IF EXISTS labnbook_backup; CREATE DATABASE labnbook_backup;" | mysql
zcat labnbook_2018-11-02_06h25m.vendredi.sql.gz | mysql -u labnbook_backup -pmdp labnbook_backup
```

Le compte mysql `labnbook` a aussi les permissions d'accès à la base `labnbook_backup`,
donc il peut envoyer des requêtes multi-bases.



## How to backup and restore

### Selected mission

```
$ php5.6 scripts/mission.php backup 703
Mission sauvegardée avec succès dans 'files/backup/mission/703'.

$ tree files/backup/mission/703
files/backup/mission/703
├── labdoc.csv
├── mission.csv
├── report_part.csv
└── ressource.csv

$ php5.6 scripts/mission.php restore 703
Mission restaurée avec succès.
```

Links between missions and teachers are not in the backup.
You have to restore them manually if they were deleted.

The files on the server (images, pdf, etc) are not in the backup.

#### Selected report

```
$ php5.6 scripts/report.php backup 12639   
Rapport sauvegardé avec succès dans 'files/backup/report/12639'.

$ tree files/backup/report/     
files/backup/report/
└── 12639
    ├── labdoc.csv
    ├── labdoc_status.csv
    ├── link_report_learner.csv
    └── report.csv

$ php5.6 scripts/report.php restore 12639                  
Rapport restauré avec succès.
```

Internal messages are not in the backup.

